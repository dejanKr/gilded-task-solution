import { Item, ShopSystem } from '../app/gilded-rose';
import { cloneDeep } from 'lodash';

const NORMAL_DEGRADE = 1;
const TWICE_DEGRADE = 2;
const THREE_TIMES_DEGRADE = 3;

describe('Shop System', function() {
  const shopSystem: ShopSystem = new ShopSystem();

  beforeEach(() => {
    shopSystem.items = [];
  });

  function daysPassed(n): Item[] {
    const previous = cloneDeep(shopSystem.items);
    for (let i = 0; i < n; ++i) {
      shopSystem.updateQuality();
    }
    return previous;
  }

  it('Item\'s name is never updated', function() {
    shopSystem.items = [
      new Item('Normal', 14, 11),
      new Item('Aged Brie', 14, 20),
      new Item('Sulfuras', 8, 8),
      new Item('Backstage passes', 14, 6),
      new Item('Conjured', 20, 6),
    ];
    const previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.name).toEqual(previous[index].name);
    });
  });

  it('Quality is never more than 50', () => {
    shopSystem.items = [
      new Item('Normal', 8, 50),
      new Item('Aged Brie', 8, 50),
      new Item('Sulfuras', 8, 50),
      new Item('Backstage passes', 8, 50),
      new Item('Conjured', 8, 50),
    ];
    daysPassed(10);
    shopSystem.items.forEach(item => {
      expect(item.quality).toBeLessThanOrEqual(50);
    });
  });

  it('Quality is never negative', () => {
    shopSystem.items = [
      new Item('Normal', 10, 0),
      new Item('Aged Brie', 10, 0),
      new Item('Sulfuras', 10, 0),
      new Item('Backstage passes', 10, 0),
      new Item('Conjured', 10, 0),
    ];
    daysPassed(10);
    shopSystem.items.forEach(item => {
      expect(item.quality).toBeGreaterThanOrEqual(0);
    });
  });

  it('Normal item quality degrade twice once the date has passed', () => {
    shopSystem.items = [
      new Item('Normal', 4, 20)
    ];
    const previous =  daysPassed(4 + 1); // 1 days passed
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
        previous[index].quality - 4 * NORMAL_DEGRADE - 1 * TWICE_DEGRADE
      );
    });
  });

  //Normal item degrade for 2 so Conjured item must degreade for 4.
  it('Conjured item quality degrade four times once the date has passed', () => {
    shopSystem.items = [
      new Item('Conjured', 4, 20)
    ];
    const previous =  daysPassed(4 + 1); // 1 days passed
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
        previous[index].quality - 4 * TWICE_DEGRADE - 2 * TWICE_DEGRADE
      );
    });
  });

  it('Aged Brie increases in quality the older it gets', () => {
    shopSystem.items = [new Item('Aged Brie', 9, 6)];
    const previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toBeGreaterThan(previous[index].quality);
    });
  });

  it('Quality and sellIn for Sulfuras never change', () => {
    shopSystem.items = [new Item('Sulfuras', 12, 6)];
    const previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.sellIn).toEqual(previous[index].sellIn);
      expect(item.quality).toEqual(previous[index].quality);
    });
  });

  it('Backstage passes increases in quality as it\'s sellIn value decreases', () => {
    shopSystem.items = [
      new Item('Backstage passes', 15, 20),
    ];
    // 15 days left
    let previous = daysPassed(5);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
        previous[index].quality + 5 * NORMAL_DEGRADE
      );
    });
    // 10 days left
    previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
        previous[index].quality + 1 * TWICE_DEGRADE
      );
    });
    // 9 days left
    previous = daysPassed(4);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
          previous[index].quality +
          4 * TWICE_DEGRADE
      );
    });
    // 5 days left
    previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
        previous[index].quality +
          1 * THREE_TIMES_DEGRADE
      );
    });
    // 4 days left
    previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
        previous[index].quality +
          1 * THREE_TIMES_DEGRADE
      );
    });
    // 3 day left
    previous = daysPassed(3);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toEqual(
          previous[index].quality +
          3 * THREE_TIMES_DEGRADE);
    });
    // 0 days left
    previous = daysPassed(1);
    shopSystem.items.forEach((item) => {
      expect(item.quality).toEqual(0);
    });
    // 1 days passed
    previous = daysPassed(1);
    shopSystem.items.forEach((item) => {
      expect(item.quality).toEqual(0);
    });
    // 2 days passed
  });

  it('Conjured item\'s quality degrade twice as fast as normal items', () => {
    shopSystem.items = [new Item('Conjured', 20, 10)];
    const previous = daysPassed(1);
    shopSystem.items.forEach((item, index) => {
      expect(item.quality).toBeLessThanOrEqual(previous[index].quality - 1 * TWICE_DEGRADE);
    });
  });
});
