const MIN_QTY = 0;
const MAX_QTY = 50;

// Item types
const AgedBrie = 'Aged Brie';
const Backstage = 'Backstage passes';
const Sulfuras = 'Sulfuras';
const Conjured = 'Conjured';

interface ItemStructure {
  name: string;
  sellIn: number;
  quality: number;
}


export class Item implements ItemStructure{
  name: string;
  sellIn: number;
  quality: number;

  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }

  canDegradeQuality(): boolean {
    return !this.canIncreaseQuality() && !this.canNotBeUpdated();
  }

  canIncreaseQuality(): boolean {
    return this.name === AgedBrie || this.name === Backstage ? true : false;
  }

  canNotBeUpdated(): boolean {
    return this.name === Sulfuras;
  }

  saleTimeExpired(): boolean {
    return this.sellIn < 0;
  }

  daysOrLessRemaining(n: number): boolean {
    return this.sellIn <= n;
  }

  degradeQuality() {
    if (this.quality > MIN_QTY) {
      this.quality -= 1;
    }
  }


  increaseQuality() {
    if (this.quality < MAX_QTY) {
      this.quality += 1;
    }
  }
}

export class ShopSystem {
  items: Array<Item> = [];

  constructor() {}

  updateItem(item) {
    this.whenQualityDegrade(item);
    this.whenQualityIncrease(item);
    this.updateSellInDays(item);
  }

  private whenQualityDegrade(item) {
    if (!item.canDegradeQuality()) return;
    item.degradeQuality();
    if (item.name === Conjured) {
      item.degradeQuality();
    }
  }

  private whenQualityIncrease(item) {
    if (!item.canIncreaseQuality()) return;
    item.increaseQuality();
    if (item.name === Backstage) {
      if (item.daysOrLessRemaining(10)) {
        item.increaseQuality();
      }
      if (item.daysOrLessRemaining(5)) {
        item.increaseQuality();
      }
    }
  }

  private updateSellInDays(item) {
    if (item.canNotBeUpdated()) return;
    item.sellIn -= 1;
    this.handleDatePassed(item);
  }

  private handleDatePassed(item) {
    if (!item.saleTimeExpired()) return;
    switch (item.name) {
      case Backstage:
        item.quality = 0; break;
      case AgedBrie:
        item.increaseQuality(); break;
      case Conjured:
        item.degradeQuality();
        item.degradeQuality(); break;
      default:
        item.degradeQuality(); break;
    }
  }

  updateQuality() {
    this.items.forEach((item) => this.updateItem(item));
    return this.items;
  }
}